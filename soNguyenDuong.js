// bài 1
document.getElementById("result").onclick = function () {
  var sum = 0;
  var i = 0;
  while (sum < 10000) {
    i++;
    sum = sum + i;
  }
  document.getElementById(
    "inNer"
  ).innerHTML = `Số nguyên dương nhỏ nhất là ${i}`;
};

// bài 2
document.getElementById("fiNish").onclick = function () {
  var x = document.getElementById("soX").value * 1;
  var n = document.getElementById("soN").value * 1;
  var sum = 0;

  for (let index = 1; index <= n; index++) {
    sum = sum + Math.pow(x, index);
  }

  document.getElementById("reSult").innerHTML = `${sum}`;
};
// bài 3
document.getElementById("tinhGiaiThua").onclick = function () {
  var soGiaiThua = document.getElementById("giaiThua").value;
  var ketQuaa = 1;

  if (soGiaiThua > 1) {
    for (var i = 1; i <= soGiaiThua; i++) {
      ketQuaa = ketQuaa * i;
    }
  }

  document.getElementById("reSult1").innerHTML = `Giai thừa là : ${ketQuaa}`;
};

// bài 4
document.getElementById("add").onclick = function () {
  var output = "";
  var count = 1;
  while (count <= 10) {
    if (count % 2 == 0) {
      var div = '<div class="alert alert-danger mt-2"> div Chẵn </div>';
    } else {
      var div = '<div class ="alert alert-primary mt-2"> div lẻ </div>';
    }

    output += div;
    count++;
  }
  document.getElementById("ketQua1").innerHTML = output;
};

// bài tập thêm
 function kiem_tra_snt (n) 
  {
    // Biến cờ hiệu
    var flag = true;
 
    // Nếu n bé hơn 2 tức là không phải số nguyên tố
    if (n < 2) {
        flag = false;
    }
    else if (n == 2) {
        flag = true;
    }
    else if (n % 2 == 0) {
        flag = false;
    }
    else {
        // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
        for (var i = 3; i <= Math.sqrt(n); i += 2)
        {
            if (n % i == 0) {
                flag = false;
                break;
            }
        }
    }
 
    return flag;
}


document.getElementById('inSoNguyenTo').onclick=function(){

  var soNguyenTo =document.getElementById('soNguyenTo').value*1;
   var sNt ='';
   for (var i = 1; i <= soNguyenTo; i++) {
    
    if (kiem_tra_snt(i)){
        sNt += i + " " ;
    }
}
document.getElementById('ketQuaSoNt').innerHTML=sNt
}
